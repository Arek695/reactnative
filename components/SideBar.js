import React from 'react';
import { Text, View, StyleSheet, ScrollView, ImageBackground, Image } from 'react-native';
import {DrawerNavigatorItems} from "react-navigation-drawer";



export default SideBar = props => (
  <ScrollView>
      <ImageBackground
      source={require("../assets/bg.jpg")}
      style={{width: undefined, padding: 16, paddingTop: 48}}
      >
          <Image source={require("../assets/avatar.png")} style={styles.profile} />
          <Text style={styles.name}>Administrator Arek</Text>

          <View style={{ flexDirection: "row" }}>
              <Text style={styles.clients}>132 clients</Text>
          </View>

      </ImageBackground>

      <View style={styles.container}>
          <DrawerNavigatorItems {...props} />
      </View>

  </ScrollView>
);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    profile: {
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: "#FFF"
    },
    name: {
        color: "#FFF",
        fontSize: 20,
        fontWeight: "800",
        marginVertical: 8
    },
    clients: {
        color: "rgba(255,255,255,0.8)",
        fontSize: 13,
        marginRight: 4,
    }
});
