import React, { Component, useState } from 'react';
import {Dimensions, KeyboardAvoidingView, StyleSheet} from 'react-native';

import { Button, Block, Text, Input } from '../components';
const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  forgot: {
    flex: 1
  }
});

class Forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {email: ''}

  }
  render() {
    const { navigation } = this.props;

    return (
        <KeyboardAvoidingView
            enabled
            behavior="padding"
            style={{ flex: 1 }}
            keyboardVerticalOffset={height * 0.2}
        >

          <Block center middle>
            <Block middle>
              {/*<Image*/}
              {/*  source={require('../assets/images/Base/Logo.png')}*/}
              {/*  style={{ height: 28, width: 102 }}*/}
              {/*/>*/}
            </Block>
            <Block flex={2.5} center>
              <Text h3 style={{ marginBottom: 6 }}>
                Forgot passowrd?
              </Text>
              <Text paragraph color="black3">
                Please enter your email. We will send a message to you.
              </Text>
              <Block center style={{ marginTop: 44 }}>

                <Input
                    full
                    name="email"
                    value={this.state.email}
                    label="Email address"
                    onChangeText={(email) => this.setState({email})}
                    style={{ marginBottom: 25 }}
                />


                <Button
                    full
                    style={{ marginBottom: 12 }}
                    // onPress={() => navigation.navigate('Overview')}
                >
                  <Text button>Send link</Text>
                </Button>
                <Text paragraph color="gray">
                  Don't have an account? <Text
                    height={18}
                    color="blue"
                    onPress={() => navigation.navigate('Register')}>
                  Sign up
                </Text>
                </Text>
              </Block>
            </Block>
          </Block>
        </KeyboardAvoidingView>
    )
  }
}

export default Forgot;
