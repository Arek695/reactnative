import React, { Component } from 'react';
import { Image, KeyboardAvoidingView, Dimensions, AsyncStorage, StyleSheet, Picker} from 'react-native';
import { Button, Block, Text, Input } from '../components';
import {Feather} from "@expo/vector-icons";
import TextInput from "react-native-web/src/exports/TextInput";

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
    invite: {
        flex: 1
    }
});

class Invite extends Component {
    constructor(props) {
        super(props);
        this.state = {choose: 'Phone'}

    }
    render() {
        const { navigation } = this.props;

        return (
            <KeyboardAvoidingView
                enabled
                behavior="padding"
                style={{ flex: 1 }}
                keyboardVerticalOffset={height * 0.2}
            >
                <Block center middle>
                    <Block middle>
                        <Feather name="send" size={76} color="blue" />
                    </Block>
                    <Block flex={2.5} center>
                        <Text h3 style={{ marginBottom: 6 }}>
                            Invite a customer
                        </Text>
                        <Text paragraph color="black3">
                            Please select
                        </Text>
                        <Picker
                            selectedValue={this.state.choose}
                            style={{height: 50, width: 350}}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({choose: itemValue})
                            }>
                            <Picker.Item label="Phone" value="Phone (+48)" />
                            <Picker.Item label="Email adress" value="Email" />
                        </Picker>
                        <Block center style={{ marginTop: 44 }}>

                            <Input
                                full
                                name={this.state.choose}
                                // value={this.state.email}
                                label={this.state.choose}
                                // onChangeText={(email) => this.setState({email})}
                                style={{ marginBottom: 25 }}
                            />
                            <Input
                                full
                                name="description"
                                // value={this.state.email}
                                label="Information"
                                // onChangeText={(email) => this.setState({email})}
                                style={{ marginBottom: 25 }}
                            />
                            <Button
                                full
                                style={{ marginBottom: 12 }}
                                onPress={() => navigation.navigate('Overview')}
                                // onPress={this.fetchdata}
                            >
                                <Text button>Invite</Text>

                            </Button>

                        </Block>
                    </Block>
                </Block>
            </KeyboardAvoidingView>
        )
    }
}

export default Invite;
