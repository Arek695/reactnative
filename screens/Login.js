import React, { Component, useState } from 'react';
import { Image, KeyboardAvoidingView, Dimensions, AsyncStorage } from 'react-native';

import { Button, Block, Text, Input } from '../components';

const { height } = Dimensions.get('window');



class Login extends Component {


  constructor(props) {
    super(props);
    this.state = {email: 'admin@abcleague.pl', password: '123123123'}

  }
  handleChange = event =>{
    this.setState({ email: event.target.value },()=>console.log(this.state))
  };
  fetchdata = event => {
    const that = this;
    event.preventDefault();
    console.log(this.state.email);
    fetch('https://abcleague.webup-dev.pl/api/login',{
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },

      body: JSON.stringify({"email": this.state.email, "password": this.state.password})
    }).then(res => res.json()).then(resData => {
      if(resData.message == 'login success') {
        that.props.navigation.push('Overview');
      }
      if(resData.message == 'login failed') {
        alert('Check email or password');
      }

    });

  }


  render() {

    const { navigation } = this.props;


    return (
      <KeyboardAvoidingView
        enabled
        behavior="padding"
        style={{ flex: 1 }}
        keyboardVerticalOffset={height * 0.2}
      >

        <Block center middle>
          <Block middle>
            {/*<Image*/}
            {/*  source={require('../assets/images/Base/Logo.png')}*/}
            {/*  style={{ height: 28, width: 102 }}*/}
            {/*/>*/}
          </Block>
          <Block flex={2.5} center>
            <Text h3 style={{ marginBottom: 6 }}>
              Sign in
            </Text>
            <Text paragraph color="black3">
              Please enter your credentials to proceed.
            </Text>
            <Block center style={{ marginTop: 44 }}>

              <Input
                full
                name="email"
                value={this.state.email}
                label="Email address"
                onChangeText={(email) => this.setState({email})}
                style={{ marginBottom: 25 }}
              />

              <Input
                full
                password
                name="password"
                label="Password"
                value={this.state.password}
                onChangeText={(password) => this.setState({password})}
                style={{ marginBottom: 25 }}
                rightLabel={
                  <Text
                    paragraph
                    color="gray"
                    onPress={() => navigation.navigate('Forgot')}
                  >
                    Forgot password?
                  </Text>
                }
              />

              <Button
                full
                style={{ marginBottom: 12 }}
                // onPress={() => navigation.navigate('Overview')}
                  onPress={this.fetchdata}
              >
                <Text button>Sign in</Text>
              </Button>
              <Text paragraph color="gray">
                Don't have an account? <Text
                  height={18}
                  color="blue"
                  onPress={() => navigation.navigate('Register')}>
                   Sign up
                </Text>
              </Text>
            </Block>
          </Block>
        </Block>
      </KeyboardAvoidingView>
    )
  }
}

export default Login;
