import React from "react";
import { createDrawerNavigator } from "react-navigation-drawer";
import {Dimensions} from 'react-native';
import OverviewStack from "./Overview";
import Analytics from "../screens/Analytics";
import Chat from "../screens/Chat";
import Service from "../screens/Service";
import Settings from "../screens/Settings";
import Invite from "../screens/Invite";
import {Feather} from "@expo/vector-icons";
import SideBar from "../components/SideBar";

export default createDrawerNavigator({
  OverviewStack: {
    screen: OverviewStack,
    navigationOptions: {
      title: "Dashboard",
      drawerIcon: ({ tintColor }) => <Feather name="user" size={16} color={tintColor} />
    }
  },
  Analytics: {
    screen: Analytics,
    navigationOptions: {
      title: "Analytics",
      drawerIcon: ({ tintColor }) => <Feather name="activity" size={16} color={tintColor} />
    }
  },
  Chat: {
    screen: Chat,
    navigationOptions: {
      title: "Messages",
      drawerIcon: ({ tintColor }) => <Feather name="message-square" size={16} color={tintColor} />
    }
  },
  Service: {
    screen: Service,
    navigationOptions: {
      title: "Service (we'll see)",
      drawerIcon: ({ tintColor }) => <Feather name="award" size={16} color={tintColor} />
    }
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      title: "Settings",
      drawerIcon: ({ tintColor }) => <Feather name="settings" size={16} color={tintColor} />
    }
  },
    Invite: {
    screen: Invite,
    navigationOptions: {
      title: "Invite",
      drawerIcon: ({ tintColor }) => <Feather name="send" size={16} color={tintColor} />
    }
    },

},{
  contentComponent: props => <SideBar {...props} />,
  drawerWidth: Dimensions.get("window").width * 0.85,
  hideStatusBar: true,

  contentOptions: {
    activeBackgroundColor: "rgba(212,118,287,0.2)",
    activeTintColor: "#531158",
  }

}
);
